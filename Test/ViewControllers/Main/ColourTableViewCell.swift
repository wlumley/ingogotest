//
//  ColourTableViewCell.swift
//  Test
//
//  Created by William Lumley on 5/6/18.
//

import UIKit
import WebKit

class ColourTableViewCell: UITableViewCell
{
    public let titleLabel: UILabel =
    {
        let label = UILabel(frame: CGRect.zero)
        label.text = "colour"
        label.textColor = UIColor.black
        label.textAlignment = .left
        
        return label
    }()
    
    public let usernameLabel: UILabel =
    {
        let label = UILabel(frame: CGRect.zero)
        label.text = "username"
        label.textColor = UIColor.black
        label.textAlignment = .left
        
        return label
    }()
    
    public let colourImageView: UIImageView =
    {
        let imageView = UIImageView(frame: CGRect.zero)
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        
        return imageView
    }()
    
    public let descriptionWebView: WKWebView =
    {
        let webView = WKWebView(frame: CGRect.zero)
        return webView
    }()
    
    //MARK: - UITableViewCell
    override init(style: UITableViewCellStyle, reuseIdentifier: String?)
    {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
    }
    
    //MARK: - Functions
    fileprivate func setup()
    {
        self.setupContraints()
    }
    
    fileprivate func setupContraints()
    {
        let views = [
            self.usernameLabel,
            self.titleLabel,
            self.colourImageView,
            self.descriptionWebView
        ]
        
        for view in views {
            view.translatesAutoresizingMaskIntoConstraints = false
            self.contentView.addSubview(view)
        }
    }
        
    /**
     Configures the UI with the properties of the provided Pattern
     - parameter pattern: The Pattern object that we will use to populate our UI
     - parameter style: The integer which represents which of our three style's we'll use
    */
    public func configure(with pattern: Pattern, style: Int)
    {
        self.contentView.backgroundColor = UIColor.white
        
        switch (style) {
        case 0:
            self.configureUIWithStyle1()
            
        case 1:
            self.configureUIWithStyle2()
            
        case 2:
            self.configureUIWithStyle3()
            
        default:()
        }
        
        self.usernameLabel.text = pattern.username
        self.titleLabel.text    = pattern.title
        self.descriptionWebView.loadHTMLString(pattern.description, baseURL: nil)
        
        if let url = pattern.imageUrl {
            self.colourImageView.setImage(from: url)
        }
    }
}
