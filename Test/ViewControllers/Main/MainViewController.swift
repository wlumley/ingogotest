//
//  ViewController.swift
//  Test
//
//  Created by William Lumley on 5/6/18.
//

import UIKit
import SwiftyXMLParser
import Starscream

class MainViewController: UIViewController
{
    ///The socket that will be used to talk to the kaazing service
    fileprivate var socket: WebSocket?
    
    ///The ReuseID for our TableView
    fileprivate let cellID = "cellID"
    
    ///The pattern that we have received from the API and processsed through the socket
    fileprivate var pattern: Pattern?
    {
        didSet {
            self.colourTableView.reloadData()
            
            guard let pattern = self.pattern else {
                return
            }
            
            self.title = pattern.title
            
            let isOnMainViewController = self.navigationController!.viewControllers.last == self
            
            //If we have one or more comments, AND we're on the MainViewController, show the details
            if pattern.commentCount > 0 && isOnMainViewController {
                
                let detailViewController = DetailViewController(pattern: pattern)
                self.navigationController?.pushViewController(detailViewController, animated: true)
            }
        }
    }
    
    unowned var mainView: MainView { return self.view as! MainView }
    unowned var colourTableView: UITableView { return self.mainView.tableView   }
    
    //MARK: - UIViewController
    override func loadView()
    {
        self.view = MainView()
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        let urlStr = "ws://demos.kaazing.com/echo"
        guard let url = URL(string: urlStr) else {
            print("Failed to create URL from: \(urlStr)")
            return
        }
        
        self.navigationController?.delegate = self
        
        self.colourTableView.register(ColourTableViewCell.self, forCellReuseIdentifier: self.cellID)
        self.colourTableView.dataSource = self
        self.colourTableView.delegate   = self
        
        self.socket = WebSocket(url: url)
        self.socket?.delegate = self
        self.socket?.connect()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}

//MARK: - Functions
extension MainViewController
{
    /**
     Pulls the 'random colour' XML from the Colour Lovers API, via our
     NetworkManager singleton
    */
    fileprivate func getRandomColours()
    {
        //Get our colours from our colourlovers API
        NetworkManager.sharedInstance.getRandomColours(onSuccess: {(data) in
            
            //We have our XML from the colourlovers API, now let's send it through the socket
            self.socket?.write(data: data)
            
            let lower = UInt32(15)
            let upper = UInt32(30)
            let random = arc4random_uniform(upper - lower) + lower
            
            //Get another set of colours in 15-30 seconds
            print("Will call getRandomColours() in \(random) seconds")
            performAfter(milliseconds: Int(random) * 1000, {
                self.getRandomColours()
            })
            
        }, onError: {(error) in
            print("Error: \(error)")
        })
    }
}

//MARK: - WebSocket DataSource
extension MainViewController: WebSocketDelegate
{
    func websocketDidConnect(socket: WebSocketClient)
    {
        print("Websocket Connected.")
        self.getRandomColours()
    }
    
    func websocketDidDisconnect(socket: WebSocketClient, error: Error?)
    {
        print("Websocket Disconnected: \(error?.localizedDescription ?? "nil")")
    }

    func websocketDidReceiveData(socket: WebSocketClient, data: Data)
    {
        print("Received Data: \(data.count)")
        
        //Great, we received our data back through the socket. Now we can parse it
        let xml = XML.parse(data)
        self.pattern = Pattern.parse(xml: xml)
    }
    
    func websocketDidReceiveMessage(socket: WebSocketClient, text: String)
    {
        //print("Received Text: \(text)")
    }
}

//MARK: - UITableView DataSource
extension MainViewController: UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.cellID) as! ColourTableViewCell
        guard let pattern = self.pattern else {
            return cell
        }
        
        let styleType = indexPath.row % 3
        cell.configure(with: pattern, style: styleType)
        
        return cell
    }
}

//MARK: - UITableView Delegate
extension MainViewController: UITableViewDelegate
{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 88.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

//MARK: - UINavigationController Delegate
extension MainViewController: UINavigationControllerDelegate
{
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationControllerOperation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning?
    {
        return ViewControllerAnimation()
    }
}
