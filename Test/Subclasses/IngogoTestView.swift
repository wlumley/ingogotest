//
//  IngogoTestView.swift
//  Test
//
//  Created by William Lumley on 5/6/18.
//

import UIKit

class IngogoTestView: UIView
{
    init()
    {
        super.init(frame: CGRect.zero)
        
        self.setup()
        self.setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
    }
    
    fileprivate func setup()
    {
        self.clipsToBounds = true
    }
    
    internal func setupConstraints()
    {
        fatalError("setupContraints() should never be called by the MatchItView superclass.")
    }
}
