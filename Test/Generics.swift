//
//  Generics.swift
//  Test
//
//  Created by William Lumley on 5/6/18.
//

import Foundation

func performOnMainThread(_ closure: @escaping ()->())
{
    DispatchQueue.main.async(execute: {
        closure()
    })
}

func performAfter(milliseconds: Int, _ closure:@escaping () -> Void)
{
    let delay = DispatchTime.now() + DispatchTimeInterval.milliseconds(milliseconds)
    DispatchQueue.main.asyncAfter(deadline: delay) {
        closure()
    }
}
