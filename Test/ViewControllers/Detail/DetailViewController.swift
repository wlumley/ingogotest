//
//  DetailViewController.swift
//  Test
//
//  Created by William on 6/6/18.
//

import UIKit

class DetailViewController: UIViewController
{
    unowned var detailView: DetailView { return self.view as! DetailView }
    unowned var infoTextView: UITextView { return self.detailView.infoTextView }
    
    public let pattern: Pattern
    
    //MARK: - UIViewController
    init(pattern: Pattern)
    {
        self.pattern = pattern
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        fatalError("init(coder:) should not be called.")
    }
    
    override func loadView()
    {
        self.view = DetailView()
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        self.infoTextView.text = self.pattern.humanReadableDescription()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
