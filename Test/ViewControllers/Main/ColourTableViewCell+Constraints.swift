//
//  ColourTableViewCell+Constraints.swift
//  Test
//
//  Created by William on 6/6/18.
//

import Foundation
import UIKit

extension ColourTableViewCell
{
    internal func configureUIWithStyle1()
    {
        //Configure Username label
        self.usernameLabel.textAlignment = .left
        
        let usernameLabelTop     = NSLayoutConstraint(item: self.usernameLabel, attribute: .topMargin, relatedBy: .equal, toItem: self.contentView, attribute: .topMargin, multiplier: 1, constant: 8)
        let usernameLabelLeading = NSLayoutConstraint(item: self.usernameLabel, attribute: .leadingMargin, relatedBy: .equal, toItem: self.contentView, attribute: .leadingMargin, multiplier: 1, constant: 1)
        
        
        //Configure Title label
        self.titleLabel.textAlignment = .right
        
        let titleLabelTop      = NSLayoutConstraint(item: self.titleLabel, attribute: .topMargin, relatedBy: .equal, toItem: self.contentView, attribute: .topMargin, multiplier: 1, constant: 8)
        let titleLabelTrailing = NSLayoutConstraint(item: self.titleLabel, attribute: .trailingMargin, relatedBy: .equal, toItem: self.contentView, attribute: .trailingMargin, multiplier: 1, constant: 1)
        
        
        //Configure ImageView
        let imageViewLeading = NSLayoutConstraint(item: self.colourImageView, attribute: .leadingMargin, relatedBy: .equal, toItem: self.contentView, attribute: .leadingMargin, multiplier: 1, constant: 1)
        let imageViewBottom  = NSLayoutConstraint(item: self.colourImageView, attribute: .bottomMargin, relatedBy: .equal, toItem: self.contentView, attribute: .bottomMargin, multiplier: 1, constant: -8)
        let imageViewWidth   = NSLayoutConstraint(item: self.colourImageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 45)
        let imageViewHeight  = NSLayoutConstraint(item: self.colourImageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 45)
        
        
        //Configure ImageView
        let webViewTrailing = NSLayoutConstraint(item: self.descriptionWebView, attribute: .trailingMargin, relatedBy: .equal, toItem: self.contentView, attribute: .trailingMargin, multiplier: 1, constant: 1)
        let webViewBottom   = NSLayoutConstraint(item: self.descriptionWebView, attribute: .bottomMargin, relatedBy: .equal, toItem: self.contentView, attribute: .bottomMargin, multiplier: 1, constant: -8)
        let webViewWidth    = NSLayoutConstraint(item: self.descriptionWebView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 45)
        let webViewHeight   = NSLayoutConstraint(item: self.descriptionWebView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 45)
        
        
        NSLayoutConstraint.activate([
            usernameLabelTop,
            usernameLabelLeading,
            
            titleLabelTop,
            titleLabelTrailing,
            
            imageViewLeading,
            imageViewBottom,
            imageViewWidth,
            imageViewHeight,
            
            webViewTrailing,
            webViewBottom,
            webViewWidth,
            webViewHeight
        ])
    }
    
    internal func configureUIWithStyle2()
    {
        //Configure Username label
        self.usernameLabel.textAlignment = .left
        
        let usernameLabelBottom  = NSLayoutConstraint(item: self.usernameLabel, attribute: .bottomMargin, relatedBy: .equal, toItem: self.contentView, attribute: .bottomMargin, multiplier: 1, constant: -8)
        let usernameLabelLeading = NSLayoutConstraint(item: self.usernameLabel, attribute: .leadingMargin, relatedBy: .equal, toItem: self.contentView, attribute: .leadingMargin, multiplier: 1, constant: 1)
        
        
        //Configure Title label
        self.titleLabel.textAlignment = .right
        
        let titleLabelBottom   = NSLayoutConstraint(item: self.titleLabel, attribute: .bottomMargin, relatedBy: .equal, toItem: self.contentView, attribute: .bottomMargin, multiplier: 1, constant: -8)
        let titleLabelTrailing = NSLayoutConstraint(item: self.titleLabel, attribute: .trailingMargin, relatedBy: .equal, toItem: self.contentView, attribute: .trailingMargin, multiplier: 1, constant: 1)
        
        
        //Configure ImageView
        let imageViewLeading = NSLayoutConstraint(item: self.colourImageView, attribute: .leadingMargin, relatedBy: .equal, toItem: self.contentView, attribute: .leadingMargin, multiplier: 1, constant: 1)
        let imageViewTop     = NSLayoutConstraint(item: self.colourImageView, attribute: .topMargin, relatedBy: .equal, toItem: self.contentView, attribute: .topMargin, multiplier: 1, constant: 8)
        let imageViewWidth   = NSLayoutConstraint(item: self.colourImageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 45)
        let imageViewHeight  = NSLayoutConstraint(item: self.colourImageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 45)
        
        
        //Configure ImageView
        let webViewTrailing = NSLayoutConstraint(item: self.descriptionWebView, attribute: .trailingMargin, relatedBy: .equal, toItem: self.contentView, attribute: .trailingMargin, multiplier: 1, constant: 1)
        let webViewTop      = NSLayoutConstraint(item: self.descriptionWebView, attribute: .topMargin, relatedBy: .equal, toItem: self.contentView, attribute: .topMargin, multiplier: 1, constant: 8)
        let webViewWidth    = NSLayoutConstraint(item: self.descriptionWebView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 45)
        let webViewHeight   = NSLayoutConstraint(item: self.descriptionWebView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 45)
        
        
        NSLayoutConstraint.activate([
            usernameLabelBottom,
            usernameLabelLeading,
            
            titleLabelBottom,
            titleLabelTrailing,
            
            imageViewLeading,
            imageViewTop,
            imageViewWidth,
            imageViewHeight,
            
            webViewTrailing,
            webViewTop,
            webViewWidth,
            webViewHeight
        ])
    }
    
    internal func configureUIWithStyle3()
    {
        //Configure Username label
        self.usernameLabel.textAlignment = .center
        
        let usernameLabelTop     = NSLayoutConstraint(item: self.usernameLabel, attribute: .topMargin, relatedBy: .equal, toItem: self.contentView, attribute: .topMargin, multiplier: 1, constant: 8)
        let usernameLabelCentreX = NSLayoutConstraint(item: self.usernameLabel, attribute: .centerX, relatedBy: .equal, toItem: self.contentView, attribute: .centerX, multiplier: 1, constant: 1)
        
        
        //Configure Title label
        self.titleLabel.textAlignment = .center
        
        let titleLabelBottom = NSLayoutConstraint(item: self.titleLabel, attribute: .bottomMargin, relatedBy: .equal, toItem: self.contentView, attribute: .bottomMargin, multiplier: 1, constant: -8)
        let titleLabelCentreX = NSLayoutConstraint(item: self.titleLabel, attribute: .centerX, relatedBy: .equal, toItem: self.contentView, attribute: .centerX, multiplier: 1, constant: 1)
        
        
        //Configure ImageView
        let imageViewLeading = NSLayoutConstraint(item: self.colourImageView, attribute: .leadingMargin, relatedBy: .equal, toItem: self.contentView, attribute: .leadingMargin, multiplier: 1, constant: 1)
        let imageViewBottom  = NSLayoutConstraint(item: self.colourImageView, attribute: .bottomMargin, relatedBy: .equal, toItem: self.contentView, attribute: .bottomMargin, multiplier: 1, constant: -8)
        let imageViewWidth   = NSLayoutConstraint(item: self.colourImageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 45)
        let imageViewHeight  = NSLayoutConstraint(item: self.colourImageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 45)
        
        
        //Configure ImageView
        let webViewTrailing = NSLayoutConstraint(item: self.descriptionWebView, attribute: .trailingMargin, relatedBy: .equal, toItem: self.contentView, attribute: .trailingMargin, multiplier: 1, constant: 1)
        let webViewBottom   = NSLayoutConstraint(item: self.descriptionWebView, attribute: .bottomMargin, relatedBy: .equal, toItem: self.contentView, attribute: .bottomMargin, multiplier: 1, constant: -8)
        let webViewWidth    = NSLayoutConstraint(item: self.descriptionWebView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 45)
        let webViewHeight   = NSLayoutConstraint(item: self.descriptionWebView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 45)
        
        
        NSLayoutConstraint.activate([
            usernameLabelTop,
            usernameLabelCentreX,
            
            titleLabelBottom,
            titleLabelCentreX,
            
            imageViewLeading,
            imageViewBottom,
            imageViewWidth,
            imageViewHeight,
            
            webViewTrailing,
            webViewBottom,
            webViewWidth,
            webViewHeight
        ])
    }
}
