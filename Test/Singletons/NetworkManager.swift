//
//  NetworkManager.swift
//  Test
//
//  Created by William Lumley on 5/6/18.
//

import UIKit
import SystemConfiguration
import SwiftyXMLParser

let DEFAULT_ERROR = "DEFAULT_ERROR."
let OFFLINE_ERROR = "This application cannot find a network connection."

class NetworkManager: NSObject
{
    //MARK: - Singleton
    override init()
    {
        super.init()
    }
    
    static let sharedInstance : NetworkManager = {
        let instance = NetworkManager()
        return instance
    }()
    
    //MARK: - Functions
    /**
     Determines if this device can access any valid internet connection
     - returns: A boolean indicative of if we can connect to the internet
    */
    class func hasConnection() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        })
            else {
                return false
        }
        
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        
        return (isReachable && !needsConnection)
    }
    
    //MARK:
    /**
     Creates a URLRequest with the provided properties
     - parameter urlString: The url (as a string) that this URLRequest will be sent to
     - parameter method: The HTTP method we want to use
     - returns: The URLRequest, configured with our provided properties
    */
    class func networkRequest(with urlString: String, method: String) -> URLRequest
    {
        guard let url = URL(string: urlString) else {
            print("Could not create URL from string: \(urlString)")
            abort()
        }
        
        let request = NSMutableURLRequest(url: url)
        request.httpMethod = method
        request.timeoutInterval = 10
        
        return request as URLRequest
    }
    
    /**
     Kicks off a URLRequest, and handles the response
     - parameter onSuccess: The closure that will be called if the request is successfully made
     - parameter onError: The closure that will be called if the request fails
    */
    func execute(request: URLRequest, with onSuccess:@escaping (Data) -> Void, with onError:@escaping (Error?, String?) -> Void)
    {
        //If we have no internet connection
        if !NetworkManager.hasConnection() {
            print("Skipping network call, no network connection.")
            
            performOnMainThread{
                onError(nil, OFFLINE_ERROR)
            }
            
            return
        }
        
        let session = URLSession(configuration: URLSessionConfiguration.default)
        
        let completionHandler = {(data : Data?, response : URLResponse?, error : Error?) in
            
            //If we can convert the NSURLResponse to an NSHTTPURLResponse
            guard let urlResponse = response as? HTTPURLResponse else {
                print("Could not create URLResponse with from request: \(request)")
                performOnMainThread {
                    onError(nil, DEFAULT_ERROR + " - Could not create URL Response")
                }
                
                return
            }
            
            print("Received URL response of \(urlResponse.statusCode) for URL: \(request.url!.absoluteString)")
            
            //We hit an error connecting to the server
            if let error = error {
                print("Could not connect to server at endpoint: \(request.url!.absoluteString). Error: \(error)")
                performOnMainThread{
                    onError(error, nil)
                }
                
                return
            }
            
            //We connected to the server fine, but there was a server-side error
            if urlResponse.statusCode != 200 {
                performOnMainThread {
                    onError(nil, DEFAULT_ERROR + " - Status code is NOT 200, instead it is: \(urlResponse.statusCode)")
                }
                
                return
            }
            
            //OK, we know there are no more errors, let's continue
            
            //If we got any data from the server, let's parse it
            guard let data = data else {
                return
            }
            
            performOnMainThread({
                onSuccess(data)
            })
        }
        
        session.dataTask(with: request, completionHandler: completionHandler).resume()
    }
}

//MARK: - API Calls
extension NetworkManager
{
    public func getRandomColours(onSuccess:@escaping(Data) -> Void, onError:@escaping(String) -> Void)
    {
        let urlStr = "http://www.colourlovers.com/api/patterns/random"
        let request = NetworkManager.networkRequest(with: urlStr, method: "GET")
        
        NetworkManager.sharedInstance.execute(request: request, with: {(data) in
            onSuccess(data)
        }, with: {(error, errorMsg) in
            if let error = error {
                onError(error.localizedDescription)
            }
            else if let errorMsg = errorMsg {
                onError(errorMsg)
            }
        })
    }
}
