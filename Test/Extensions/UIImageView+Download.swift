//
//  UIImageView+Download.swift
//  Test
//
//  Created by William on 6/6/18.
//

import UIKit

extension UIImageView
{
    /**
     Kicks off an image download, and compiles then assigns the image to the ImageView
     - parameter url: The URL at which our image is located
    */
    public func setImage(from url: URL)
    {
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error {
                print("Failed to extract image data: \(error)")
                return
            }
            
            guard let data = data else {
                print("Downloaded data is nil.")
                return
            }
            
            guard let urlResponse = response as? HTTPURLResponse else {
                print("Could not create URLResponse")
                return
            }
            
            if urlResponse.statusCode != 200 {
                print("Response status code is: \(urlResponse.statusCode)")
                return
            }
            
            guard let image = UIImage(data: data) else {
                print("Failed to compile data to image.")
                return
            }
            
            performOnMainThread {
                self.image = image
            }
        }.resume()
    }
}
