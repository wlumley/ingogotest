//
//  SplashView.swift
//  Test
//
//  Created by William Lumley on 5/6/18.
//

import UIKit

class MainView: IngogoTestView
{
    public let tableView: UITableView =
    {
        let tableView = UITableView(frame: CGRect.zero, style: .plain)
        return tableView
    }()
    
    override func setupConstraints()
    {
        let views = [
            self.tableView
        ]
        
        for view in views {
            view.translatesAutoresizingMaskIntoConstraints = false
            self.addSubview(view)
        }
        
        let width   = NSLayoutConstraint(item: self.tableView, attribute: .width, relatedBy: .equal, toItem: self, attribute: .width, multiplier: 1, constant: 1)
        let height  = NSLayoutConstraint(item: self.tableView, attribute: .height, relatedBy: .equal, toItem: self, attribute: .height, multiplier: 1, constant: 1)
        let centreX = NSLayoutConstraint(item: self.tableView, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 1)
        let centreY = NSLayoutConstraint(item: self.tableView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 1)

        NSLayoutConstraint.activate([
            width,
            height,
            centreX,
            centreY
        ])
    }
}
