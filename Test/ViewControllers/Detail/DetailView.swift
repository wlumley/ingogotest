//
//  DetailView.swift
//  Test
//
//  Created by William on 6/6/18.
//

import Foundation
import UIKit

class DetailView: IngogoTestView
{
    public let infoTextView: UITextView =
    {
        let textView = UITextView(frame: CGRect.zero)
        textView.textColor = UIColor.black
        textView.textAlignment = .left
        
        return textView
    }()
    
    override func setupConstraints()
    {
        let views = [
            self.infoTextView
        ]
        
        for view in views {
            view.translatesAutoresizingMaskIntoConstraints = false
            self.addSubview(view)
        }
        
        let width   = NSLayoutConstraint(item: self.infoTextView, attribute: .width, relatedBy: .equal, toItem: self, attribute: .width, multiplier: 1, constant: 1)
        let height  = NSLayoutConstraint(item: self.infoTextView, attribute: .height, relatedBy: .equal, toItem: self, attribute: .height, multiplier: 1, constant: 1)
        let centreX = NSLayoutConstraint(item: self.infoTextView, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 1)
        let centreY = NSLayoutConstraint(item: self.infoTextView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 1)
        
        NSLayoutConstraint.activate([
            width,
            height,
            centreX,
            centreY
        ])
    }
}
