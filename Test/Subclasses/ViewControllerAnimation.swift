//
//  ViewControllerAnimation.swift
//  Test
//
//  Created by William on 6/6/18.
//

import Foundation
import UIKit

class ViewControllerAnimation: NSObject, UIViewControllerAnimatedTransitioning
{
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval
    {
        return 0.44
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning)
    {
        guard let toView = transitionContext.view(forKey: .to) else {
            return
        }
        
        let containerView = transitionContext.containerView
        
        toView.alpha = 0.0
        containerView.addSubview(toView)
        
        UIView.animate(withDuration: 0.44, animations: {
            toView.alpha = 1.0
        }, completion: {(_) in
            transitionContext.completeTransition(true)
        })
    }
}
