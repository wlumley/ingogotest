//
//  Colour.swift
//  Test
//
//  Created by William Lumley on 5/6/18.
//

import Foundation
import SwiftyXMLParser

class Pattern
{
    public var id          = 0
    public var title       = ""
    public var username    = ""
    public var description = ""
    
    public var imageUrl: URL?
    
    public var commentCount = 0
    
    
    public var rank       = 0
    public var viewCount  = 0
    public var voteCount  = 0
    public var heartCount = 0
    
    public var dateCreated = Date()
    
    public var colours = [String]()
    
    init()
    {
        
    }
    
    /**
     Parses the XML and iterates through it, creating and returning a Pattern object from the data
     - parameter xml: The XML that we will use to create our Pattern object
     - returns: A Pattern object, populated using the data in the XML. Will return nil if XML data cannot be used
    */
    public static func parse(xml: XML.Accessor) -> Pattern?
    {
        guard let patterns = xml["patterns"].element else {
            return nil
        }
        
        guard let patternTree = patterns.childElements.first else {
            return nil
        }
        
        let pattern = Pattern()
        
        //Iterate over every element in our Pattern XML tree, and populate our Pattern object
        for childElement in patternTree.childElements {
            let key   = childElement.name
            let value = childElement.text ?? ""
            
            //print("\(key): \(value)")
            
            switch (key) {
            case "title":
                pattern.title = value
                
            case "userName":
                pattern.username = value
                
            case "description":
                pattern.description = value
                
            case "imageUrl":
                let url = URL(string: value)
                pattern.imageUrl = url
                
            case "numComments":
                pattern.commentCount = Int(value) ?? 0

            case "numVotes":
                pattern.voteCount = Int(value) ?? 0

            case "numViews":
                pattern.viewCount = Int(value) ?? 0

            case "numHearts":
                pattern.heartCount = Int(value) ?? 0

            case "rank":
                pattern.rank = Int(value) ?? 0

                
            default:()
            }
        }
        
        return pattern
    }
    
    /**
     Creates a String that is the human readable format of this Pattern object
     - returns: The String that will be a human readable format, describing the Pattern object
    */
    public func humanReadableDescription() -> String
    {
        var desc = ""
        desc += "Title: \(self.title)\n"
        desc += "Username: \(self.username)\n"
        desc += "Rank: \(self.rank)\n"
        desc += "Number of Comments: \(self.commentCount)\n"
        desc += "Number of Votes: \(self.voteCount)\n"
        desc += "Number of Views: \(self.viewCount)\n"
        desc += "Number of Hearts: \(self.heartCount)\n"
        
        return desc
    }
}
